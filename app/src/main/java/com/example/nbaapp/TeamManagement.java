package com.example.nbaapp;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TeamManagement extends AppCompatActivity {

    RosterManager rosterManager;
    TextView allP, incP, acP, freeAgent, onContract;

    ArrayList<DataClass> roster;
    ArrayList<DataClass> injury;
    ArrayList<DataClass> contract;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_team_management);

        LinearLayout linearLayout = findViewById(R.id.main);
        AnimationDrawable animationDrawable = (AnimationDrawable) linearLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2500);
        animationDrawable.setExitFadeDuration(5000);
        animationDrawable.start();

        rosterManager = RosterManager.getInstance();

        allP = findViewById(R.id.allPlayers);
        acP = findViewById(R.id.activePlayers);
        incP = findViewById(R.id.inactivePlayers);
        freeAgent = findViewById(R.id.freeAgent);
        onContract = findViewById(R.id.onContractPlayers);

        roster = new ArrayList<>();
        injury = new ArrayList<>();
        contract = new ArrayList<>();

        fetchDataFromFirebase();
    }

    private void fetchDataFromFirebase() {
        DatabaseReference rosterRef = FirebaseDatabase.getInstance().getReference("roster");
        DatabaseReference injuryRef = FirebaseDatabase.getInstance().getReference("injuryReserve");
        DatabaseReference contractRef = FirebaseDatabase.getInstance().getReference("contractQueue");

        rosterRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                roster.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    DataClass data = dataSnapshot.getValue(DataClass.class);
                    if (data != null) {
                        roster.add(data);
                    }
                }
                updateTextViews();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Roster", "Failed to read data from Firebase", error.toException());
            }
        });

        injuryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                injury.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    DataClass data = dataSnapshot.getValue(DataClass.class);
                    if (data != null) {
                        injury.add(data);
                    }
                }
                updateTextViews();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Injury", "Failed to read data from Firebase", error.toException());
            }
        });

        contractRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                contract.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    DataClass data = dataSnapshot.getValue(DataClass.class);
                    if (data != null) {
                        contract.add(data);
                    }
                }
                updateTextViews();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Contract", "Failed to read data from Firebase", error.toException());
            }
        });
    }

    private void updateTextViews() {
        int freeAgents = contract.size();
        int sumIncPlayers = injury.size() + contract.size();
        int sumAcPlayers = roster.size() - (injury.size() + contract.size());
        int sumContractedPlayers = roster.size() - freeAgents;

        int noPlayers = roster.size();

        allP.setText(String.valueOf(noPlayers));
        acP.setText(String.valueOf(sumAcPlayers));
        incP.setText(String.valueOf(sumIncPlayers));
        freeAgent.setText(String.valueOf(freeAgents));
        onContract.setText(String.valueOf(sumContractedPlayers));
    }
}
