package com.example.nbaapp;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

// Glide module implementation with required annotation
@GlideModule
public class MyGlideAppModule extends AppGlideModule {
}

