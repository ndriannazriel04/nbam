package com.example.nbaapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Stack;

public class InjuryStack extends AppCompatActivity {

    private RecyclerView recyclerView;
    private InjuryAdapter injuryAdapter;
    private ArrayList<DataClass> injuryList;
    private RosterManager rosterManager;
    private Context context;

    Button remove;

    private static final String KEY_INJURY_LIST = "injury_list";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_injury_stack);

        LinearLayout linearLayout = findViewById(R.id.injuryLayout);
        AnimationDrawable animationDrawable = (AnimationDrawable) linearLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2500);
        animationDrawable.setExitFadeDuration(5000);
        animationDrawable.start();

        // Get the singleton instance of RosterManager
        rosterManager = RosterManager.getInstance();
        context = this;



        /*if (savedInstanceState != null) {
            injuryList = savedInstanceState.getParcelableArrayList(KEY_INJURY_LIST);
        } else {
            injuryList = getInjuredPlayersFromRosterManager();
        }*/
        // Fetch data from Firebase
        fetchDataFromFirebase();

        injuryList = new ArrayList<>();

        recyclerView = findViewById(R.id.injuryStack);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3)); // 3 columns
        injuryAdapter = new InjuryAdapter(this, injuryList);
        recyclerView.setAdapter(injuryAdapter);



        remove = findViewById(R.id.removeInjuredPlayerButton);

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!injuryList.isEmpty()) {
                    // Remove the top player from the local injury list
                    DataClass data = injuryList.remove(0);
                    String playerId = data.getPlayerName();
                    removePlayerFromFirebase(playerId);

                    // Remove the player from the injury reserve stack and add them back to the roster
                    rosterManager.removeFromInjuryReserve(data);
                    injuryAdapter.notifyDataSetChanged();



                }
            }
        });

    }

    private void removePlayerFromFirebase(String playerId) {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("injuryReserve");

        if (playerId != null) {
            myRef.child(playerId).removeValue().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Log.d("Firebase", "Player info removed successfully.");
                    Toast.makeText(context, "Player removed successfully.", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("Firebase", "Failed to remove player info.");
                    Toast.makeText(context, "Failed to remove player.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void fetchDataFromFirebase() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("injuryReserve");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                injuryList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    DataClass data = dataSnapshot.getValue(DataClass.class);
                    if (data != null) {
                        injuryList.add(data);
                    }
                }
                injuryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Roster", "Failed to read data from Firebase", error.toException());
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(KEY_INJURY_LIST, injuryList);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        injuryList = savedInstanceState.getParcelableArrayList(KEY_INJURY_LIST);
    }

    private ArrayList<DataClass> getInjuredPlayersFromRosterManager() {
        ArrayList<DataClass> injuredPlayers = new ArrayList<>();
        Stack<DataClass> injuryReserveStack = rosterManager.getInjuryReserve();

        for (DataClass player : injuryReserveStack) {  // Iterating over a Stack<DataClass>
            injuredPlayers.add(player);
        }
        return injuredPlayers;
    }
}
