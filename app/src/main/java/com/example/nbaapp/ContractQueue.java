package com.example.nbaapp;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Queue;
import java.util.Stack;

public class ContractQueue extends AppCompatActivity {

    RosterManager rosterManager;
    RecyclerView recyclerView;
    Button renew;
    ContractAdapter contractAdapter;

    private ArrayList<DataClass> contractList;

    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //EdgeToEdge.enable(this);
        setContentView(R.layout.activity_contract_queue);

        LinearLayout linearLayout = findViewById(R.id.main);
        AnimationDrawable animationDrawable = (AnimationDrawable) linearLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2500);
        animationDrawable.setExitFadeDuration(5000);
        animationDrawable.start();

        rosterManager = RosterManager.getInstance();
        context=this;

        // Fetch data from Firebase
        fetchDataFromFirebase();

        //contractList = getContractPlayersFromRosterManager();
        contractList = new ArrayList<>();

        recyclerView = findViewById(R.id.contractQueue);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3)); // 3 columns
        contractAdapter = new ContractAdapter(this,contractList);
        recyclerView.setAdapter(contractAdapter);

        renew = findViewById(R.id.renewButton);

        renew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!contractList.isEmpty()) {
                    // Remove the top player from the local injury list
                    DataClass data = contractList.remove(0);
                    String playerId = data.getPlayerName();
                    removePlayerFromFirebase(playerId);

                    // Remove the player from the injury reserve stack and add them back to the roster
                    rosterManager.removeFromContractExtensionQueue(data);
                    contractAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    private void removePlayerFromFirebase(String playerId) {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("contractQueue");

        if (playerId != null) {
            myRef.child(playerId).removeValue().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Log.d("Firebase", "Player info removed successfully.");
                    Toast.makeText(context, "Player removed successfully.", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("Firebase", "Failed to remove player info.");
                    Toast.makeText(context, "Failed to remove player.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void fetchDataFromFirebase() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("contractQueue");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                contractList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    DataClass data = dataSnapshot.getValue(DataClass.class);
                    if (data != null) {
                        contractList.add(data);
                    }
                }
                contractAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Roster", "Failed to read data from Firebase", error.toException());
            }
        });
    }

    private ArrayList<DataClass> getContractPlayersFromRosterManager() {
        ArrayList<DataClass> contractPlayers = new ArrayList<>();
        Queue<DataClass> contractQueue = rosterManager.getContractPlayers();

        for (DataClass player : contractQueue) {  // Iterating over a Stack<DataClass>
            contractPlayers.add(player);
        }
        return contractPlayers;
    }
}