package com.example.nbaapp;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.example.nbaapp.databinding.ActivityMainBinding;

import java.util.ArrayList;

public class Roster extends AppCompatActivity {

    RecyclerView recyclerView;
    TextView textView1, textView2, guards, forwards, centers;
    Button button, button2, button3, button4;
    MyAdapter adapter;
    ArrayList<DataClass> dataList;
    RosterManager rosterManager;
    SwipeRefreshLayout swipeRefreshLayout;

    ActivityMainBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roster);

        LinearLayout linearLayout = findViewById(R.id.rosterLayout);
        AnimationDrawable animationDrawable = (AnimationDrawable) linearLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2500);
        animationDrawable.setExitFadeDuration(5000);
        animationDrawable.start();

        rosterManager = RosterManager.getInstance();

        textView1 = findViewById(R.id.currentSalary);
        textView2 = findViewById(R.id.playerStatus);
        guards = findViewById(R.id.guardsTV);
        forwards = findViewById(R.id.forwardsTV);
        centers = findViewById(R.id.centersTV);
        button = findViewById(R.id.removeButton);
        button2 = findViewById(R.id.GoToStack);
        button3 = findViewById(R.id.GoToQueue);
        button4 = findViewById(R.id.GoToStatus);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout2);

        // Fetch saved salary from Firebase
        fetchCurrentSalaryFromFirebase();

        // Fetch data from Firebase
        fetchDataFromFirebase();

        dataList = new ArrayList<>();

        recyclerView = findViewById(R.id.teamList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyAdapter(this, dataList, MyAdapter.MODE_ROSTER, rosterManager);
        recyclerView.setAdapter(adapter);


        swipeRefreshLayout.setOnRefreshListener(this::fetchDataFromFirebase);

        button.setOnClickListener(view -> {
            DataClass data = dataList.remove(dataList.size() - 1);
            String playerId = data.getPlayerName();
            removePlayerFromFirebase(playerId); // Pass the player ID to the method
            RosterManager.getInstance().removePlayerFromRoster(data);

            adapter.notifyDataSetChanged(); // Notify adapter about the item removal
        });

        button2.setOnClickListener(view -> {
            Intent intent = new Intent(Roster.this, InjuryStack.class);
            startActivity(intent);
        });

        button3.setOnClickListener(view -> {
            Intent intent = new Intent(Roster.this, ContractQueue.class);
            startActivity(intent);
        });

        button4.setOnClickListener(view -> {
            Intent intent = new Intent(Roster.this, TeamManagement.class);
            startActivity(intent);
        });
    }

    private void removePlayerFromFirebase(String playerId) {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("roster");

        if (playerId != null) {
            myRef.child(playerId).removeValue().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Log.d("Firebase", "Player info removed successfully.");
                    //Toast.makeText(context, "Player removed successfully.", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("Firebase", "Failed to remove player info.");
                    //Toast.makeText(context, "Failed to remove player.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void fetchDataFromFirebase() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("roster");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                dataList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    DataClass data = dataSnapshot.getValue(DataClass.class);
                    if (data != null) {
                        dataList.add(data);
                    }
                }
                adapter.notifyDataSetChanged();
                updateSalaryAndStatus();
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Roster", "Failed to read data from Firebase", error.toException());
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    public void updateSalaryAndStatus() {
        // Existing code...

        // Calculate total salary of all players
        int totalSalary = calculateTotalSalary();

        // Update current salary TextView
        textView1.setText(String.valueOf(totalSalary));
        textView2.setText(isFull() ? "FULL" : "NOT FULL");
        updatePositions();

        // Save the updated salary to Firebase
        saveSalaryToFirebase();
        saveNumberOfPlayersToFirebase();
    }

    private boolean isFull(){
        return dataList.size() >= 15;
    }

    private int calculateTotalSalary() {
        int totalSalary = 0;
        for (DataClass player : dataList) {
            totalSalary += player.getSalary();
        }
        return totalSalary;
    }

    private void saveNumberOfPlayersToFirebase() {
        DatabaseReference playersRef = FirebaseDatabase.getInstance().getReference("NoOfPlayers");

        int numberOfPlayers = dataList.size();
        playersRef.setValue(numberOfPlayers).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d("Firebase", "Number of players saved successfully.");
            } else {
                Log.d("Firebase", "Failed to save number of players.");
            }
        });
    }

    private void saveSalaryToFirebase() {
        DatabaseReference salaryRef = FirebaseDatabase.getInstance().getReference("currentSalary");


        //int salary = rosterManager.getCurrentSalary();
        int salary = calculateTotalSalary();
        salaryRef.setValue(salary).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d("Firebase", "Salary saved successfully.");
            } else {
                Log.d("Firebase", "Failed to save salary.");
            }
        });
    }

    private void fetchCurrentSalaryFromFirebase() {
        DatabaseReference salaryRef = FirebaseDatabase.getInstance().getReference("currentSalary");
        salaryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Long currentSalary = snapshot.getValue(Long.class);
                if (currentSalary != null) {
                    RosterManager.getInstance().setCurrentSalary(currentSalary); // Update the current salary in RosterManager
                    textView1.setText(String.valueOf(currentSalary)); // Update the UI
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Roster", "Failed to read current salary from Firebase", error.toException());
            }
        });
    }


    public void updatePositions() {
        int guardsCnt = 0, forwardsCnt = 0, centersCnt = 0;

        for (DataClass player : dataList) {
            switch (player.getPosition()) {
                case "SG":
                case "PG":
                    guardsCnt++;
                    break;
                case "SF":
                case "PF":
                    forwardsCnt++;
                    break;
                default:
                    centersCnt++;
                    break;
            }
        }

        guards.setText("Guards: " + guardsCnt);
        forwards.setText("Forwards: " + forwardsCnt);
        centers.setText("Centers: " + centersCnt);

        // Set text color for guards
        guards.setTextColor(getResources().getColor(guardsCnt == 1 ? R.color.Red : guardsCnt >= 2 ? R.color.green : R.color.white));

        // Set text color for forwards
        forwards.setTextColor(getResources().getColor(forwardsCnt == 1 ? R.color.Red : forwardsCnt >= 2 ? R.color.green : R.color.white));

        // Set text color for centers
        centers.setTextColor(getResources().getColor(centersCnt == 1 ? R.color.Red : centersCnt >= 2 ? R.color.green : R.color.white));
    }
}
