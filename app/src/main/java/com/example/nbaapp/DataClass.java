package com.example.nbaapp;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class DataClass implements Parcelable {

    private Long height;
    private Long weight;
    private String age;
    private String points;
    private String steals;
    private String assists;
    private String position;
    private Long salary;
    private String rebounds;
    private String blocks;
    private String playerName;
    private String profilePhoto;
    private String injuryDescription; // New field

    public DataClass() {
    }

    protected DataClass(Parcel in) {
        height = in.readLong();
        weight = in.readLong();
        age = in.readString();
        points = in.readString();
        steals = in.readString();
        assists = in.readString();
        position = in.readString();
        salary = in.readLong();
        rebounds = in.readString();
        blocks = in.readString();
        playerName = in.readString();
        profilePhoto = in.readString();
        injuryDescription = in.readString(); // Read the new field
    }

    public static final Creator<DataClass> CREATOR = new Creator<DataClass>() {
        @Override
        public DataClass createFromParcel(Parcel in) {
            return new DataClass(in);
        }

        @Override
        public DataClass[] newArray(int size) {
            return new DataClass[size];
        }
    };

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        DataClass other = (DataClass) obj;
        // Customize this method based on the attributes you want to compare
        return Objects.equals(this.playerName, other.playerName) &&
                Objects.equals(this.position, other.position) &&
                // Compare other attributes as needed
                // For primitive types like int, use == for comparison
                Objects.equals(this.height, other.height) &&
                Objects.equals(this.weight, other.weight);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(height);
        dest.writeLong(weight);
        dest.writeString(age);
        dest.writeString(points);
        dest.writeString(steals);
        dest.writeString(assists);
        dest.writeString(position);
        dest.writeLong(salary);
        dest.writeString(rebounds);
        dest.writeString(blocks);
        dest.writeString(playerName);
        dest.writeString(profilePhoto);
        dest.writeString(injuryDescription); // Write the new field
    }

    @Override
    public int describeContents() {
        return 0;
    }

    // Getters and setters...
    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getSteals() {
        return steals;
    }

    public void setSteals(String steals) {
        this.steals = steals;
    }

    public String getAssists() {
        return assists;
    }

    public void setAssists(String assists) {
        this.assists = assists;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    public String getRebounds() {
        return rebounds;
    }

    public void setRebounds(String rebounds) {
        this.rebounds = rebounds;
    }

    public String getBlocks() {
        return blocks;
    }

    public void setBlocks(String blocks) {
        this.blocks = blocks;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getInjuryDescription() {
        return injuryDescription;
    }

    public void setInjuryDescription(String injuryDescription) {
        this.injuryDescription = injuryDescription;
    }
}
