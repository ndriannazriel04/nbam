package com.example.nbaapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    public static final int MODE_PLAYER_LIST = 1;
    public static final int MODE_ROSTER = 2;

    RosterManager rosterManager;

    Context context;
    ArrayList<DataClass> list;
    int mode;

    public void setFilteredList(ArrayList<DataClass> filteredList){
        this.list = filteredList;
        notifyDataSetChanged();
    }

    public MyAdapter(Context context, ArrayList<DataClass> list, int mode,RosterManager rosterManager) {
        this.context = context;
        this.list = list;
        this.mode = mode;
        this.rosterManager = rosterManager;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.player, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        DataClass dataClass = list.get(position);

        holder.Age.setText("Age: " + String.valueOf(dataClass.getAge()));
        holder.Points.setText("Points: " + String.valueOf(dataClass.getPoints()));
        holder.Assists.setText("Assists: " + String.valueOf(dataClass.getAssists()));
        holder.Steals.setText("Steals: " + String.valueOf(dataClass.getSteals()));
        holder.Weight.setText("Weight: " + String.valueOf(dataClass.getWeight()));
        holder.Height.setText("Height: " + String.valueOf(dataClass.getHeight()));
        holder.Blocks.setText("Blocks: " + String.valueOf(dataClass.getBlocks()));
        holder.Rebounds.setText("Rebounds: " + String.valueOf(dataClass.getRebounds()));
        holder.Salary.setText("Salary: " + String.valueOf(dataClass.getSalary()));

        holder.PlayerName.setText(dataClass.getPlayerName());
        holder.Position.setText("Position:" + dataClass.getPosition());

        String imageURL = dataClass.getProfilePhoto();
        Log.d("Image URL", "URL: " + imageURL);

        Glide.with(holder.itemView.getContext())
                .load(imageURL)
                .placeholder(R.drawable.player_dunking) // Optional placeholder
                .into(holder.profileImage);

        holder.itemView.setOnClickListener(view -> {
            if (mode == MODE_PLAYER_LIST) {
                showPlayerListDialog(holder, position);
            } else if (mode == MODE_ROSTER) {
                showRosterDialog(holder, position);
            }
        });

        if (dataClass.getPlayerName().equals("LeBron James")) {
            holder.itemView.setOnLongClickListener(view -> {
                MediaPlayer mediaPlayer = MediaPlayer.create(view.getContext(), R.raw.youaremysunshine);
                mediaPlayer.start();
                return true; // Return true to indicate that the long click was handled
            });
        } else {
            holder.itemView.setOnLongClickListener(null);
        }

        if (imageURL == null || imageURL.isEmpty()) {
            holder.profileImage.setImageResource(R.drawable.player_dunking); // Default placeholder
        }

        boolean isInjury = rosterManager.getInjuryReserve().contains(dataClass);
        boolean isContractEnd = rosterManager.getContractPlayers().contains(dataClass);

        if (isInjury && isContractEnd) {
            // Set a different color or pattern if both conditions are true
            holder.circle.setBackgroundColor(ContextCompat.getColor(context, R.color.lavender));
        } else if (isInjury) {
            holder.circle.setBackgroundColor(ContextCompat.getColor(context, R.color.Red));
        } else if (isContractEnd) {
            holder.circle.setBackgroundColor(ContextCompat.getColor(context, R.color.teal_700));
        } else {
            holder.circle.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
        }

    }


    private void showPlayerListDialog(MyViewHolder holder, int position) {
        View dialogView = LayoutInflater.from(context).inflate(R.layout.activity_pop_up_view, null);
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(dialogView)
                .create();

        Button cancelButton = dialogView.findViewById(R.id.cancelButton);
        Button confirmButton = dialogView.findViewById(R.id.confirmButton);

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataClass data = list.get(position);
                boolean added = RosterManager.getInstance().addPlayer(data);
                if(added){
                    MediaPlayer mp = MediaPlayer.create(context,R.raw.bell);
                    mp.start();

                    list.remove(position);
                    notifyDataSetChanged();

                    Intent intent = new Intent(context, Roster.class);
                    context.startActivity(intent);

                    if (context instanceof Roster) {
                        ((Roster) context).updateSalaryAndStatus();// Update the UI
                        //((Roster) context).adapter.notifyDataSetChanged();
                    }

                    //savePlayerToFirebase(data); // Save player info to Firebase
                }
                dialog.dismiss();
            }
        });

        cancelButton.setOnClickListener(view -> dialog.dismiss());
        dialog.show();
    }

    private void showRosterDialog(MyViewHolder holder, int position) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bottomsheetpopup);

        LinearLayout remove = dialog.findViewById(R.id.removePlayerButton);
        LinearLayout addInjury = dialog.findViewById(R.id.injuryButton);
        LinearLayout addContract = dialog.findViewById(R.id.addQueue);
        LinearLayout cancel = dialog.findViewById(R.id.cancel);

        // Get the data for the selected player
        DataClass data = list.get(position);
        TextView PlayerName = dialog.findViewById(R.id.playerNameTV);

        // Set data to the views
        PlayerName.setText(data.getPlayerName());

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataClass data = list.get(position);
                String playerId = data.getPlayerName(); // Get the player ID
                removePlayerFromFirebase1(playerId); // Pass the player ID to the method
                RosterManager.getInstance().removePlayerFromRoster(data);

                removePlayerFromFirebase2(playerId);
                RosterManager.getInstance().removeFromInjuryReserve(data);

                removePlayerFromFirebase3(playerId);
                RosterManager.getInstance().removeFromContractExtensionQueue(data);

                if (context instanceof Roster) {
                    ((Roster) context).updateSalaryAndStatus();
                }

                list.remove(position); // Remove the item from the list
                notifyDataSetChanged(); // Notify adapter about the item removal
                dialog.dismiss();
            }
        });


        addInjury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataClass data = list.get(position);
                RosterManager.getInstance().addToInjuryReserve(data, "Injury Description");

                // Save the player's injury status
                savePlayerInjuryStatus(data.getPlayerName(), true);

                notifyDataSetChanged();
                dialog.dismiss();
            }
        });


        addContract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataClass data = list.get(position);
                RosterManager.getInstance().addToContractExtensionQueue(data);

                savePlayerContractStatus(data.getPlayerName(), true);

                notifyDataSetChanged();

                notifyDataSetChanged();
                dialog.dismiss();
            }
        });



        dialog.show();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setGravity(Gravity.BOTTOM);
    }

    private void removePlayerFromFirebase1(String playerId) {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("roster");

        if (playerId != null) {
            String sanitizedPlayerId = sanitizePlayerName(playerId);
            myRef.child(sanitizedPlayerId).removeValue().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Log.d("Firebase", "Player info removed successfully.");
                    Toast.makeText(context, "Player removed successfully.", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("Firebase", "Failed to remove player info.");
                    Toast.makeText(context, "Failed to remove player.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void removePlayerFromFirebase2(String playerId) {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("injuryReserve");

        if (playerId != null) {
            String sanitizedPlayerId = sanitizePlayerName(playerId);
            myRef.child(sanitizedPlayerId).removeValue().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Log.d("Firebase", "Player info removed successfully.");
                    Toast.makeText(context, "Player removed successfully.", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("Firebase", "Failed to remove player info.");
                    Toast.makeText(context, "Failed to remove player.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void removePlayerFromFirebase3(String playerId) {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("contractQueue");

        if (playerId != null) {
            String sanitizedPlayerId = sanitizePlayerName(playerId);
            myRef.child(sanitizedPlayerId).removeValue().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Log.d("Firebase", "Player info removed successfully.");
                    Toast.makeText(context, "Player removed successfully.", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("Firebase", "Failed to remove player info.");
                    Toast.makeText(context, "Failed to remove player.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    // Sanitize player name to remove invalid characters for Firebase Database paths
    private String sanitizePlayerName(String playerName) {
        return playerName.replaceAll("[.$\\[\\]#\\/]", "_");
    }



    private void savePlayerInjuryStatus(String playerName, boolean isInjured) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PlayerInjuries", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(playerName, isInjured);
        editor.apply();
    }

    private void savePlayerContractStatus(String playerName, boolean isContractEnd) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PlayerContracts", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(playerName, isContractEnd);
        editor.apply();
    }





    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView profileImage;
        //ImageView circle;
        //Button myButton;
        View circle;
        TextView Age, Assists, Height, Points, Steals, Weight, Position, Salary, Rebounds, Blocks, PlayerName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            profileImage = itemView.findViewById(R.id.profileImage);
            circle = itemView.findViewById(R.id.statusColor);
            //myButton = itemView.findViewById(R.id.statusColor);
            Age = itemView.findViewById(R.id.AgeTV);
            Points = itemView.findViewById(R.id.PointsTV);
            Height = itemView.findViewById(R.id.HeightTV);
            Weight = itemView.findViewById(R.id.WeightTV);
            Assists = itemView.findViewById(R.id.AssistsTV);
            Steals = itemView.findViewById(R.id.StealsTV);
            Position = itemView.findViewById(R.id.PositionTV);
            Salary = itemView.findViewById(R.id.SalaryTV);
            Rebounds = itemView.findViewById(R.id.ReboundsTV);
            Blocks = itemView.findViewById(R.id.BlocksTV);
            PlayerName = itemView.findViewById(R.id.playerNameTV);
        }
    }
}
