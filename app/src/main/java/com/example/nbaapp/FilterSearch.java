package com.example.nbaapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class FilterSearch extends AppCompatActivity {

    EditText editTextHeight,editTextWeight;
    ImageView buttonFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_search);

        // Get references to the EditText fields
        EditText editTextHeight = findViewById(R.id.editTextHeight);
        EditText editTextWeight = findViewById(R.id.editTextWeight);
        EditText editTextPosition = findViewById(R.id.editTextPosition);
        EditText editTextSalary = findViewById(R.id.editTextSalary);
        ImageView buttonFilter = findViewById(R.id.buttonFilter);

        buttonFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get input from EditText fields
                String heightInput = editTextHeight.getText().toString();
                String weightInput = editTextWeight.getText().toString();
                String salaryInput = editTextSalary.getText().toString();
                String positionInput = editTextPosition.getText().toString();

                // Parse numbers only if input is not empty, otherwise use a default
                int minHeight = heightInput.isEmpty() ? 0 : Integer.parseInt(heightInput);
                int minWeight = weightInput.isEmpty() ? 0 : Integer.parseInt(weightInput);
                int minSalary = salaryInput.isEmpty() ? 0 : Integer.parseInt(salaryInput);

                // Prepare the intent with filters
                Intent resultIntent = new Intent(FilterSearch.this, PlayerList.class);

                // Pass non-empty values to the intent
                resultIntent.putExtra("minHeight", minHeight);
                resultIntent.putExtra("minWeight", minWeight);
                resultIntent.putExtra("minSalary", minSalary);

                // Pass position only if provided
                if (!positionInput.isEmpty()) {
                    resultIntent.putExtra("position", positionInput);
                }

                startActivity(resultIntent);
            }
        });



    }
}