package com.example.nbaapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class PlayerList extends AppCompatActivity {

    private static final String KEY_DATA_LIST = "dataList";
    private static final String SHARED_PREFS_KEY = "playerlist_shared_prefs";
    private static final String DATA_LIST_KEY = "data_list_key";

    RecyclerView recyclerView;
    DatabaseReference databaseReference;
    MyAdapter myAdapter;
    ArrayList<DataClass> list;
    RosterManager rosterManager;
    SwipeRefreshLayout swipeRefreshLayout;
    private SearchView searchView;
    ImageView imageView;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_list);

        LinearLayout linearLayout = findViewById(R.id.playerListLayout);
        AnimationDrawable animationDrawable = (AnimationDrawable) linearLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2500);
        animationDrawable.setExitFadeDuration(5000);
        animationDrawable.start();

        rosterManager = RosterManager.getInstance();

        searchView = findViewById(R.id.searchView);
        searchView.clearFocus();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filterList(newText);
                return true;
            }
        });

        imageView = findViewById(R.id.advSearch);
        imageView.setOnClickListener(view -> {
            Intent intent = new Intent(PlayerList.this, FilterSearch.class);
            startActivity(intent);
        });

        // Initialize RosterManager
        rosterManager = new RosterManager("TeamName", "TeamCity", 20000);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::refreshData);

        if (savedInstanceState != null) {
            list = savedInstanceState.getParcelableArrayList(KEY_DATA_LIST);
        }

        recyclerView = findViewById(R.id.playerList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        // Define the space between items
        int verticalSpace = 50; // 50 pixels of space
        VerticalSpaceItemDecoration itemDecoration = new VerticalSpaceItemDecoration(verticalSpace);

        // Add the item decoration to the RecyclerView
        recyclerView.addItemDecoration(itemDecoration);

        list = new ArrayList<>();
        int minHeight = getIntent().getIntExtra("minHeight", 0);
        int minWeight = getIntent().getIntExtra("minWeight", 0);
        int minSalary = getIntent().getIntExtra("minSalary", 0);
        String position = getIntent().getStringExtra("position");
        loadPlayerData(minHeight, minWeight, minSalary, position);

        myAdapter = new MyAdapter(this, list, MyAdapter.MODE_PLAYER_LIST, rosterManager);
        recyclerView.setAdapter(myAdapter);


    }


    private void filterList(String text) {
        ArrayList<DataClass> filteredList = new ArrayList<>();
        for (DataClass item : list) {
            if (item.getPlayerName().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }

        if (filteredList.isEmpty()) {
            Toast.makeText(this, "No Player Found", Toast.LENGTH_SHORT).show();
        } else {
            myAdapter.setFilteredList(filteredList);
        }
    }

    private void loadPlayerData(int minHeight, int minWeight, int minSalary, String POS) {
        databaseReference = FirebaseDatabase.getInstance().getReference().child("player");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                Log.d("FirebaseData", "DataSnapshot content: " + dataSnapshot.toString());

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Log.d("FirebaseData", "Snapshot: " + snapshot.toString());

                    DataClass dataClass = new DataClass();

                    String profilePhoto = snapshot.child("profilePhoto").getValue(String.class);
                    Long height = snapshot.child("Height(cm)").getValue(Long.class);
                    Long weight = snapshot.child("Weight(kg)").getValue(Long.class);
                    String age = snapshot.child("Age").getValue(String.class);
                    String points = snapshot.child("Points").getValue(String.class);
                    String assists = snapshot.child("Assist").getValue(String.class);
                    String steals = snapshot.child("Steal").getValue(String.class);
                    String rebound = snapshot.child("Rebound").getValue(String.class);
                    Long salary = snapshot.child("Salary").getValue(Long.class);
                    String block = snapshot.child("Block").getValue(String.class);
                    String position = snapshot.child("POS").getValue(String.class);
                    String name = snapshot.child("Name").getValue(String.class);

                    boolean matches = true;

                    if (height == null || height < minHeight) {
                        matches = false;
                    }

                    if (weight == null || weight < minWeight) {
                        matches = false;
                    }

                    if (salary == null || salary < minSalary) {
                        matches = false;
                    }

                    if (POS != null && !POS.equals(position)) {
                        matches = false;
                    }

                    if (matches) {
                        dataClass.setProfilePhoto(profilePhoto);
                        dataClass.setPosition(position);
                        dataClass.setPlayerName(name);

                        dataClass.setHeight(height);
                        dataClass.setWeight(weight);
                        dataClass.setAge(age);
                        dataClass.setPoints(points);
                        dataClass.setAssists(assists);
                        dataClass.setSteals(steals);
                        dataClass.setRebounds(rebound);
                        dataClass.setSalary(salary);
                        dataClass.setBlocks(block);

                        list.add(dataClass);
                    }
                }

                myAdapter.notifyDataSetChanged();

                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(PlayerList.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void refreshData() {
        int minHeight = getIntent().getIntExtra("minHeight", 0);
        int minWeight = getIntent().getIntExtra("minWeight", 0);
        int minSalary = getIntent().getIntExtra("minSalary", 0);
        String position = getIntent().getStringExtra("position");
        loadPlayerData(minHeight, minWeight, minSalary, position);
    }

}
