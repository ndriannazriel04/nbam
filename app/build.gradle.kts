plugins {
    alias(libs.plugins.androidApplication)
    id ("com.google.gms.google-services")
}

android {
    namespace = "com.example.nbaapp"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.example.nbaapp"
        minSdk = 26
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    buildFeatures{

        viewBinding = true
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {

    implementation(libs.appcompat)
    implementation(libs.material)
    implementation(libs.activity)
    implementation(libs.constraintlayout)
    implementation(libs.firebase.storage)
    implementation(libs.firebase.firestore)
    testImplementation(libs.junit)
    androidTestImplementation(libs.ext.junit)
    androidTestImplementation(libs.espresso.core)
    implementation (platform("com.google.firebase:firebase-bom:32.8.1"))
    implementation("com.google.firebase:firebase-database")

    implementation ("com.google.firebase:firebase-storage:20.0.0")
    implementation ("com.github.bumptech.glide:glide:4.15.1") // Glide
    annotationProcessor("com.github.bumptech.glide:compiler:4.15.1")

    implementation("com.squareup.picasso:picasso:2.71828")
    implementation ("com.google.code.gson:gson:2.8.8")

    implementation ("androidx.swiperefreshlayout:swiperefreshlayout:1.1.0")

}